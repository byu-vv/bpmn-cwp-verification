//START
inline START_BehaviorModel(){
    skip
}
//T01
inline T01_BehaviorModel(){
    if
        :: true -> var1 = True
        :: true -> var1 = False
    fi
    updateState()
}

//GATEWAY1
inline GATEWAY1_BehaviorModel(){
    skip
}

//T02
inline T02_BehaviorModel(){
    if
        :: true -> var2 = True
        :: true -> var2 = False
    fi
    updateState()
}

//GATEWAY2
inline GATEWAY2_BehaviorModel(){
    skip
}

//T03
inline T03_BehaviorModel(){
    if
        :: true -> var3 = True
        :: true -> var3 = False
    fi
    updateState()
}

//GATEWAY3
inline GATEWAY3_BehaviorModel(){
    skip
}

//T04
inline T04_BehaviorModel(){
    if
        :: true -> var4 = True
        :: true -> var4 = False
    fi
    updateState()
}

//GATEWAY4
inline GATEWAY4_BehaviorModel(){
    skip
}

//T05
inline T05_BehaviorModel(){
    if
        :: true -> var5 = True
        :: true -> var5 = False
    fi
    updateState()
}

//GATEWAY5
inline GATEWAY5_BehaviorModel(){
    skip
}

//T999
inline T999_BehaviorModel(){
    updateState()
}
//END
inline END_BehaviorModel(){
    skip
}
