//start1
inline start1_BehaviorModel(){
    skip
}

//T01
inline T01_BehaviorModel(){
    semaphore = act1
    updateState()
}

//gateway1
inline gateway1_BehaviorModel(){
    skip
}

//T02
inline T02_BehaviorModel(){
    fin1 = True
    updateState()
}

//end1
inline end1_BehaviorModel(){
    skip
}

//start2
inline start2_BehaviorModel(){
    skip
}

//T03
inline T03_BehaviorModel(){
    semaphore = act2
    updateState()
}

//gateway2
inline gateway2_BehaviorModel(){
    skip
}

//T04
inline T04_BehaviorModel(){
    fin2 = True
    updateState()
}

//end2
inline end2_BehaviorModel(){
    skip
}

//start3
inline start3_BehaviorModel(){
    skip
}

//T05
inline T05_BehaviorModel(){
    semaphore = act3
    updateState()
}

//gateway3
inline gateway3_BehaviorModel(){
    skip
}

//T06
inline T06_BehaviorModel(){
    fin3 = True
    updateState()
}

//end3
inline end3_BehaviorModel(){
    skip
}

//start4
inline start4_BehaviorModel(){
    skip
}

//T07
inline T07_BehaviorModel(){
    semaphore = act4
    updateState()
}

//gateway4
inline gateway4_BehaviorModel(){
    skip
}

//T08
inline T08_BehaviorModel(){
    fin4 = True
    updateState()
}

//end4
inline end4_BehaviorModel(){
    skip
}

//start5
inline start5_BehaviorModel(){
    skip
}

//T09
inline T09_BehaviorModel(){
    semaphore = act5
    updateState()
}

//gateway5
inline gateway5_BehaviorModel(){
    skip
}

//T10
inline T10_BehaviorModel(){
    fin5 = True
    updateState()
}

//end5
inline end5_BehaviorModel(){
    skip
}

//start6
inline start6_BehaviorModel(){
    skip
}

//T11
inline T11_BehaviorModel(){
    semaphore = act6
    updateState()
}

//gateway6
inline gateway6_BehaviorModel(){
    skip
}

//T12
inline T12_BehaviorModel(){
    fin6 = True
    updateState()
}

//end6
inline end6_BehaviorModel(){
    skip
}

//start7
inline start7_BehaviorModel(){
    skip
}

//T13
inline T13_BehaviorModel(){
    semaphore = act7
    updateState()
}

//gateway7
inline gateway7_BehaviorModel(){
    skip
}

//T14
inline T14_BehaviorModel(){
    fin7 = True
    updateState()
}

//end7
inline end7_BehaviorModel(){
    skip
}

//start8
inline start8_BehaviorModel(){
    skip
}

//T15
inline T15_BehaviorModel(){
    semaphore = act8
    updateState()
}

//gateway8
inline gateway8_BehaviorModel(){
    skip
}

//T16
inline T16_BehaviorModel(){
    fin8 = True
    updateState()
}

//end8
inline end8_BehaviorModel(){
    skip
}

//start9
inline start9_BehaviorModel(){
    skip
}

//T17
inline T17_BehaviorModel(){
    semaphore = act9
    updateState()
}

//gateway9
inline gateway9_BehaviorModel(){
    skip
}

//T18
inline T18_BehaviorModel(){
    fin9 = True
    updateState()
}

//end9
inline end9_BehaviorModel(){
    skip
}

//start10
inline start10_BehaviorModel(){
    skip
}

//T19
inline T19_BehaviorModel(){
    semaphore = act10
    updateState()
}

//gateway10
inline gateway10_BehaviorModel(){
    skip
}

//T20
inline T20_BehaviorModel(){
    fin10 = True
    updateState()
}

//end10
inline end10_BehaviorModel(){
    skip
}

