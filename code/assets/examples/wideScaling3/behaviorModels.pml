//start1
inline start1_BehaviorModel(){
    skip
}

//T01
inline T01_BehaviorModel(){
    semaphore = act1
    updateState()
}

//gateway1
inline gateway1_BehaviorModel(){
    skip
}

//T02
inline T02_BehaviorModel(){
    fin1 = True
    updateState()
}

//end1
inline end1_BehaviorModel(){
    skip
}

//start2
inline start2_BehaviorModel(){
    skip
}

//T03
inline T03_BehaviorModel(){
    semaphore = act2
    updateState()
}

//gateway2
inline gateway2_BehaviorModel(){
    skip
}

//T04
inline T04_BehaviorModel(){
    fin2 = True
    updateState()
}

//end2
inline end2_BehaviorModel(){
    skip
}

//start3
inline start3_BehaviorModel(){
    skip
}

//T05
inline T05_BehaviorModel(){
    semaphore = act3
    updateState()
}

//gateway3
inline gateway3_BehaviorModel(){
    skip
}

//T06
inline T06_BehaviorModel(){
    fin3 = True
    updateState()
}

//end3
inline end3_BehaviorModel(){
    skip
}

