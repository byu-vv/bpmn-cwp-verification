const byte paymentAmount paymentAmount 253
const byte belowPaymentAmount belowPaymentAmount 252
const byte noRetryPayment noRetryPayment 254
const enum INIT INIT
const enum other other
const enum buyerName buyerName
const enum sellerName sellerName
const enum agreed agreed
const enum failed failed
const byte pendingPayment pendingPayment 255
const enum pending pending
const enum noRetry noRetry
enum terms terms pending [agreed, failed, pending, noRetry]
enum backpackOwner backpackOwner sellerName [buyerName, sellerName]
enum paymentOwner paymentOwner buyerName [sellerName, buyerName]
byte paymentOffered paymentOffered pendingPayment [252-255]