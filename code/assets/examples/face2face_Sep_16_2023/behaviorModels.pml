
//Start7
inline Start7_BehaviorModel(){
	skip
}
//T1
inline T1_BehaviorModel(){
	if
		:: true -> backpackOwner = sellerName
	fi
	if
		:: true -> paymentOwner = buyerName
	fi
	updateState()
}
//T1B
inline T1B_BehaviorModel(){
	updateState()
}
//both
inline both_BehaviorModel(){
	skip
}
//T2
inline T2_BehaviorModel(){
	if
		:: true -> terms = agreed
		:: true -> terms = failed
	fi
	updateState()
}
//T3
inline T3_BehaviorModel(){
	if
		:: true -> paymentOffered = paymentAmount
		:: true -> paymentOffered = belowPaymentAmount
	fi
	updateState()
}
//end_both
inline end_both_BehaviorModel(){
	updateState()
}
//payment_and_terms_agreed
inline payment_and_terms_agreed_BehaviorModel(){
	skip
}
//T6
inline T6_BehaviorModel(){
	updateState()
}
//both1
inline both1_BehaviorModel(){
	skip
}
//T7a
inline T7a_BehaviorModel(){
	if
		:: true -> paymentOwner = sellerName
	fi
	updateState()
}
//T7b
inline T7b_BehaviorModel(){
	if
		:: true -> backpackOwner = buyerName
	fi
	updateState()
}
//end_both1
inline end_both1_BehaviorModel(){
	updateState()
}
//Purchase_Completed
inline Purchase_Completed_BehaviorModel(){
	skip
}
//Purchase_Failed
inline Purchase_Failed_BehaviorModel(){
	skip
}
//T4
inline T4_BehaviorModel(){
	if
		:: true -> paymentOffered = paymentAmount
		:: true -> paymentOffered = belowPaymentAmount
		:: true -> paymentOffered = noRetryPayment
	fi
	updateState()
}
//T5
inline T5_BehaviorModel(){
	if
		:: true -> terms = agreed
		:: true -> terms = failed
		:: true -> terms = noRetry
	fi
	updateState()
}

init{
	skip
}