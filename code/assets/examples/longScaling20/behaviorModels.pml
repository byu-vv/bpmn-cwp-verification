//START
inline START_BehaviorModel(){
    skip
}
//T01
inline T01_BehaviorModel(){
    if
        :: true -> var1 = True
        :: true -> var1 = False
    fi
    updateState()
}

//GATEWAY1
inline GATEWAY1_BehaviorModel(){
    skip
}

//T02
inline T02_BehaviorModel(){
    if
        :: true -> var2 = True
        :: true -> var2 = False
    fi
    updateState()
}

//GATEWAY2
inline GATEWAY2_BehaviorModel(){
    skip
}

//T03
inline T03_BehaviorModel(){
    if
        :: true -> var3 = True
        :: true -> var3 = False
    fi
    updateState()
}

//GATEWAY3
inline GATEWAY3_BehaviorModel(){
    skip
}

//T04
inline T04_BehaviorModel(){
    if
        :: true -> var4 = True
        :: true -> var4 = False
    fi
    updateState()
}

//GATEWAY4
inline GATEWAY4_BehaviorModel(){
    skip
}

//T05
inline T05_BehaviorModel(){
    if
        :: true -> var5 = True
        :: true -> var5 = False
    fi
    updateState()
}

//GATEWAY5
inline GATEWAY5_BehaviorModel(){
    skip
}

//T06
inline T06_BehaviorModel(){
    if
        :: true -> var6 = True
        :: true -> var6 = False
    fi
    updateState()
}

//GATEWAY6
inline GATEWAY6_BehaviorModel(){
    skip
}

//T07
inline T07_BehaviorModel(){
    if
        :: true -> var7 = True
        :: true -> var7 = False
    fi
    updateState()
}

//GATEWAY7
inline GATEWAY7_BehaviorModel(){
    skip
}

//T08
inline T08_BehaviorModel(){
    if
        :: true -> var8 = True
        :: true -> var8 = False
    fi
    updateState()
}

//GATEWAY8
inline GATEWAY8_BehaviorModel(){
    skip
}

//T09
inline T09_BehaviorModel(){
    if
        :: true -> var9 = True
        :: true -> var9 = False
    fi
    updateState()
}

//GATEWAY9
inline GATEWAY9_BehaviorModel(){
    skip
}

//T10
inline T10_BehaviorModel(){
    if
        :: true -> var10 = True
        :: true -> var10 = False
    fi
    updateState()
}

//GATEWAY10
inline GATEWAY10_BehaviorModel(){
    skip
}

//T11
inline T11_BehaviorModel(){
    if
        :: true -> var11 = True
        :: true -> var11 = False
    fi
    updateState()
}

//GATEWAY11
inline GATEWAY11_BehaviorModel(){
    skip
}

//T12
inline T12_BehaviorModel(){
    if
        :: true -> var12 = True
        :: true -> var12 = False
    fi
    updateState()
}

//GATEWAY12
inline GATEWAY12_BehaviorModel(){
    skip
}

//T13
inline T13_BehaviorModel(){
    if
        :: true -> var13 = True
        :: true -> var13 = False
    fi
    updateState()
}

//GATEWAY13
inline GATEWAY13_BehaviorModel(){
    skip
}

//T14
inline T14_BehaviorModel(){
    if
        :: true -> var14 = True
        :: true -> var14 = False
    fi
    updateState()
}

//GATEWAY14
inline GATEWAY14_BehaviorModel(){
    skip
}

//T15
inline T15_BehaviorModel(){
    if
        :: true -> var15 = True
        :: true -> var15 = False
    fi
    updateState()
}

//GATEWAY15
inline GATEWAY15_BehaviorModel(){
    skip
}

//T16
inline T16_BehaviorModel(){
    if
        :: true -> var16 = True
        :: true -> var16 = False
    fi
    updateState()
}

//GATEWAY16
inline GATEWAY16_BehaviorModel(){
    skip
}

//T17
inline T17_BehaviorModel(){
    if
        :: true -> var17 = True
        :: true -> var17 = False
    fi
    updateState()
}

//GATEWAY17
inline GATEWAY17_BehaviorModel(){
    skip
}

//T18
inline T18_BehaviorModel(){
    if
        :: true -> var18 = True
        :: true -> var18 = False
    fi
    updateState()
}

//GATEWAY18
inline GATEWAY18_BehaviorModel(){
    skip
}

//T19
inline T19_BehaviorModel(){
    if
        :: true -> var19 = True
        :: true -> var19 = False
    fi
    updateState()
}

//GATEWAY19
inline GATEWAY19_BehaviorModel(){
    skip
}

//T20
inline T20_BehaviorModel(){
    if
        :: true -> var20 = True
        :: true -> var20 = False
    fi
    updateState()
}

//GATEWAY20
inline GATEWAY20_BehaviorModel(){
    skip
}

//T999
inline T999_BehaviorModel(){
    updateState()
}
//END
inline END_BehaviorModel(){
    skip
}
