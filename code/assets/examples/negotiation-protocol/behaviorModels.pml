//SS00
inline SS00_BehaviorModel(){
	skip
}

//SG00
inline SG00_BehaviorModel(){
	skip
}

//Receive_Buyer_Message
inline Receive_Buyer_Message_BehaviorModel(){
	skip
}

//SG01
inline SG01_BehaviorModel(){
	skip
}

//TS00
inline TS00_BehaviorModel(){
	if
		:: true -> seller = offer
		:: true -> seller = reject
		:: true -> seller = accept
	fi
	updateState()
}

//SG02
inline SG02_BehaviorModel(){
	skip
}

//Send_Seller_Message
inline Send_Seller_Message_BehaviorModel(){
	skip
}

//TS01
inline TS01_BehaviorModel(){
	if
		:: buyer == reject -> seller = reject
		:: buyer == accept -> seller = accept
		:: else -> assert false;
	fi
	updateState()
}

//SG03
inline SG03_BehaviorModel(){
	skip
}

//SE01
inline SE01_BehaviorModel(){
	skip
}

//SE00
inline SE00_BehaviorModel(){
	skip
}

//BS00
inline BS00_BehaviorModel(){
	skip
}

//TB00
inline TB00_BehaviorModel(){
	updateState()
}

//BG02
inline BG02_BehaviorModel(){
	skip
}

//Send_Buyer_Message
inline Send_Buyer_Message_BehaviorModel(){
	skip
}

//Receive_Seller_Message
inline Receive_Seller_Message_BehaviorModel(){
	skip
}

//BG01
inline BG01_BehaviorModel(){
	skip
}

//TB01
inline TB01_BehaviorModel(){
	if
		:: true -> buyer = offer
		:: true -> buyer = reject
		:: true -> buyer = accept
	fi
	updateState()
}

//TB02
inline TB02_BehaviorModel(){
	if
		:: seller == reject -> buyer = reject
		:: seller == accept -> buyer = accept
		:: else -> assert false
	fi
	updateState()
}

//BG03
inline BG03_BehaviorModel(){
	skip
}

//BE01
inline BE01_BehaviorModel(){
	skip
}

//BE00
inline BE00_BehaviorModel(){
	skip
}