//start1
inline start1_BehaviorModel(){
    skip
}

//T01
inline T01_BehaviorModel(){
    semaphore = act1
    updateState()
}

//gateway1
inline gateway1_BehaviorModel(){
    skip
}

//T02
inline T02_BehaviorModel(){
    fin1 = True
    updateState()
}

//end1
inline end1_BehaviorModel(){
    skip
}

