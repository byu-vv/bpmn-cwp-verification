
//T01
inline T01_BehaviorModel(){
	updateState()
}
//Start1
inline Start1_BehaviorModel(){
	skip
}
//Xor1
inline Xor1_BehaviorModel(){
	skip
}
//T02
inline T02_BehaviorModel(){
	if
		:: paymentOffered == belowPaymentAmount -> paymentOffered = paymentAmount
        :: paymentOffered == belowPaymentAmount -> paymentOffered = belowPaymentAmount
        :: paymentOffered == belowPaymentAmount -> paymentOffered = noRetryPayment
		:: else -> skip
	fi
    if
        :: paymentOffered == pendingPayment -> paymentOffered = paymentAmount
        :: paymentOffered == pendingPayment -> paymentOffered = belowPaymentAmount
		:: else -> skip
    fi
	updateState()
}
//Xor2
inline Xor2_BehaviorModel(){
	skip
}
//T03
inline T03_BehaviorModel(){
	if
    	:: terms == failed -> terms = agreed
    	:: terms == failed -> terms = failed
    	:: terms == failed -> terms = noRetry
		:: else -> skip
	fi
    if
        :: terms == pending -> terms = agreed
        :: terms == pending -> terms = failed
		:: else -> skip
    fi
	updateState()
}
//Xor3
inline Xor3_BehaviorModel(){
	skip
}
//Xor4
inline Xor4_BehaviorModel(){
	skip
}
//End1
inline End1_BehaviorModel(){
	skip
}
//End2
inline End2_BehaviorModel(){
	skip
}
//T04
inline T04_BehaviorModel(){
	updateState()
}
//Start2
inline Start2_BehaviorModel(){
	skip
}
//T06
inline T06_BehaviorModel(){
	if
		:: paymentOffered == belowPaymentAmount -> paymentOffered = paymentAmount
        :: paymentOffered == belowPaymentAmount -> paymentOffered = belowPaymentAmount
        :: paymentOffered == belowPaymentAmount -> paymentOffered = noRetryPayment
		:: else -> skip
	fi
    if
        :: paymentOffered == pendingPayment -> paymentOffered = paymentAmount
        :: paymentOffered == pendingPayment -> paymentOffered = belowPaymentAmount
		:: else -> skip
    fi
	updateState()
}
//Xor5
inline Xor5_BehaviorModel(){
	skip
}
//Xor6
inline Xor6_BehaviorModel(){
	skip
}
//End4
inline End4_BehaviorModel(){
	skip
}
//T07
inline T07_BehaviorModel(){
	if
    	:: terms == failed -> terms = agreed
    	:: terms == failed -> terms = failed
    	:: terms == failed -> terms = noRetry
		:: else -> skip
	fi
    if
        :: terms == pending -> terms = agreed
        :: terms == pending -> terms = failed
		:: else -> skip
    fi
	updateState()
}
//Xor7
inline Xor7_BehaviorModel(){
	skip
}
//Xor8
inline Xor8_BehaviorModel(){
	skip
}
//End5
inline End5_BehaviorModel(){
	skip
}
//T08
inline T08_BehaviorModel(){
	if
		:: true -> paymentOwner = sellerName
	fi
	updateState()
}
//Start3
inline Start3_BehaviorModel(){
	skip
}
//T05
inline T05_BehaviorModel(){
	updateState()
}
//End3
inline End3_BehaviorModel(){
	skip
}
//Start4
inline Start4_BehaviorModel(){
	skip
}
//T09
inline T09_BehaviorModel(){
	if
		:: true -> backpackOwner = buyerName
	fi
	updateState()
}
//End6
inline End6_BehaviorModel(){
	skip
}

//End7
inline End7_BehaviorModel(){
	skip
}

//End8
inline End8_BehaviorModel(){
	skip
}

//Intermediate1
inline Intermediate1_BehaviorModel(){
	skip
}

