from ExpressionTree.ExpressionTree import *
class TestExpressionTree():
    def test_1(self):
        self.ET1 = AND(Paren(OR(Equal("a", "7"), NOT(Equal("b", "3")))), Equal("c", "5"))
        self.assertEqual("( (a == 7) || !(b == 3) ) && (c == 5)", str(self.ET1))
    
    def test_2(self):
        self.ET1 = OR(Paren(AND(AND(Equal("e", "f"), Equal("a", "7")), NOT(Equal("b", "3")))), Equal("c", "5"))
        self.assertEqual("( (e == f) && (a == 7) && !(b == 3) ) || (c == 5)", str(self.ET1))
