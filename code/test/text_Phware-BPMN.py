from BPMN.BPMN import StartNode, Label, Flow, ActivityNode, IntermediateNode, EndNode, XorNode, Node, Macro, Msg, MsgIntermediateNode, TimerIntermediateNode

def main():
    
    # Flow of the creation for a new node is as follows:
    # 1) Create node
    # 2) Attach node to its incoming flow
    # 3) Create Outgoing flow
    # 4) Connect Outgoing flow to node
    # 5) Deal with incoming/outgoing messages

    #### BEGIN FIRST PROCESS (CLINICIAN) ####

    #START 1 (TOP LEFT CORNER)
    S1 = StartNode(Label("Start1"))
    F1 = Flow(Label("pt + COVID-19"))
    S1.addOutFlow(F1)
    
    #ACTIVITY 1
    A1 = ActivityNode(Label("01 - Doc-Nurse examine pt"))
    F1.setToNode(A1)
    F2 = Flow(Label("N/A"))
    A1.addOutFlow(F2)


    #XOR5
    Xor5 = XorNode(Label("Xor5 severity"))
    F2.setToNode(Xor5)
    F3 = Flow(Label("sevLvl > 2"))
    F4 = Flow(Label("discharge criteria met"))
    F5 = Flow(Label("sevLvl <= 2"))
    Xor5.addOutFlow(F3)
    Xor5.addOutFlow(F4)
    Xor5.addOutFlow(F5)
    
    #ACTIVITY 3
    A3 = ActivityNode(Label("03 - Doc admit pt to hospital or ICU care"))
    F3.setToNode(A3)
    F6 = Flow(Label("N/A"))
    
    #XOR4
    Xor4 = XorNode(Label("Xor4 fatality"))
    F6.setToNode(Xor4)
    F7 = Flow(Label("expired"))
    F8 = Flow(Label("survived"))
    Xor4.addOutFlow(F7)
    Xor4.addOutFlow(F8)
    
    #END1
    End1 = EndNode(Label("pt expired"))
    F7.setToNode(End1)
    
    #XOR4 REPEAT DESTINATIONS
    F8.setToNode(A1)
    
    #END2
    End2 = EndNode(Label("pt discharged"))
    F4.setToNode(End2)
    
    #ACTIVITY2
    A2 = ActivityNode(Label("02 - Doc orders home care with Phware and exams"))
    M3 = Msg(Label("N/A"))
    F5.setToNode(A2)
    F9 = Flow(Label("N/A"))
    A2.addOutFlow(F9)
    A2.addOutMsg(M3)
    
    #INTERMEDIATE1
    I1 = MsgIntermediateNode(Label("Seconds Catch AI"))
    M1 = Msg(Label("N/A"), I1)
    F9.setToNode(I1)
    F10 = Flow(Label("N/A"))
    I1.addOutFlow(F10)
    
    #XOR8
    Xor8 = XorNode(Label("Xor8 alert"))
    F10.setToNode(Xor8)
    F11 = Flow(Label("Yes"))
    F12 = Flow(Label("No"))
    Xor8.addOutFlow(F11)
    Xor8.addOutFlow(F12)

    #ACTIVITY 7a
    A7a = ActivityNode(Label("07a - Doc-Nurse review alert, vitals, exam schedule, and home situation"))
    F11.setToNode(A7a)
    F13 = Flow(Label("N/A"))
    A7a.addOutFlow(F13)

    #XOR9
    Xor9 = XorNode(Label("Xor9 exam orders"))
    F13.setToNode(Xor9)
    F14 = Flow(Label("no orders"))
    F15 = Flow(Label("examTime = now"))
    F16 = Flow(Label("alert confirmed - trndSevLvl > careCapLvl"))
    F17 = Flow(Label("routine exam due"))
    Xor9.addOutFlow(F14) 
    Xor9.addOutFlow(F15) 
    Xor9.addOutFlow(F16) 
    Xor9.addOutFlow(F17)

    #XOR11
    Xor11 = XorNode(Label("Xor11 any examTime = now"))
    F15.setToNode(Xor11)
    F18 = Flow(Label("examTime != now"))
    F19 = Flow(Label("examTime = now"))
    Xor11.addOutFlow(F18)
    Xor11.addOutFlow(F19)
    
    #INTERMEDIATE 2
    I2 = MsgIntermediateNode(Label("seconds catch ex pt"))
    M2 = Msg(Label("N/A"), I2)
    F19.setToNode(I2)
    F20 = Flow(Label("N/A"))
    I2.addOutFlow(F20)

    #INTERMEDIATE 2 REPEAT DESTINATIONS
    F20.setToNode(A1)

    #XOR11 REPEAT DESTINATIONS
    F18.setToNode(I1)

    #ACTIVITY 8a
    A8a = ActivityNode(Label("08a - Scheduler set up exam now and inform pt"))
    F16.setToNode(A8a)
    F21 = Flow(Label("examTime = now"))
    A8a.addOutFlow(F21)

    #ACTIVITY 8a REPEAT DESTINATIONS
    F21.setToNode(Xor11)

    #ACTIVITY 8b
    A8b = ActivityNode(Label("08b - Scheduler schedule routine exam and inform pt"))
    F17.setToNode(A8b)
    F22 = Flow(Label("examTime != now"))
    A8b.addOutFlow(F22)

    #ACTIVITY 8b REPEAT DESTINATIONS
    F22.setToNode(Xor11)

    #XOR9 REPEAT DESTINATION
    F14.setToNode(I1)

    #INTERMEDIATE 3
    I3 = TimerIntermediateNode(Label("N/A"))
    F12.setToNode(I3)
    F23 = Flow(Label("hours delay"))
    I3.addOutFlow(F23)

    #ACTIVITY 7b
    A7b = ActivityNode(Label("07b - Doc-Nurse review vitals and exam schedule"))
    F23.setToNode(A7b)
    F24 = Flow(Label("N/A"))
    A7b.addOutFlow(F24)

    #XOR10
    Xor10 = XorNode(Label("Xor10 exam orders"))
    F24.setToNode(Xor10)
    F25 = Flow(Label("trndSevLvl > careCapLvl"))
    F26 = Flow(Label("routine exam due"))
    F27 = Flow(Label("examTime = now"))
    F28 = Flow(Label("no orders"))
    Xor10.addOutFlow(F25)
    Xor10.addOutFlow(F26)
    Xor10.addOutFlow(F27)
    Xor10.addOutFlow(F28)

    #XOR10 REPEAT DESTINATIONS
    F25.setToNode(A8a)
    F26.setToNode(A8b)
    F27.setToNode(Xor11)
    F28.setToNode(I1)
    
    #### END FIRST PROCESS (CLINICIAN) ####
    


    #### BEGIN SECOND PROCESS (SERVER) ####
    
    #START 113
    S2 = StartNode(Label("Start113"))
    F29 = Flow(Label("N/A"))
    S2.addOutFlow(F29)
    M4 = Msg(Label("N/A"), S2)

    #ACTIVITY 06
    A6 = ActivityNode(Label("06 - AI analyze vitals"))
    F29.setToNode(A6)
    F30 = Flow(Label("N/A"))
    A6.addOutFlow(F30)
    A6.addOutMsg(M2)

    #END 232
    End3 = EndNode(Label("End232"))
    F30.setToNode(End3)

    #### END SECOND PROCESS (SERVER) ####



    #### BEGIN THIRD PROCESS (PATIENT-CAREGIVER) ####

    #START 170
    S170 = StartNode(Label("Start170"))
    F31 = Flow(Label("N/A"))
    S170.addOutFlow(F31)
    M3.setToNode(S170)

    #ACTIVITY 04
    A4 = ActivityNode(Label("04 - Pt or care-giver get-install Phware"))
    F31.setToNode(A4)
    F32 = Flow(Label("N/A"))
    A4.addOutFlow(F32)

    #ACTIVITY 05
    A5 = ActivityNode(Label("05 - Pt or care-giver follow orders to record vitals"))
    F32.setToNode(A5)
    F33 = Flow(Label("N/A"))
    A5.addOutFlow(F33)

    #XOR6
    Xor6 = XorNode(Label("Xor6 fatality"))
    F33.setToNode(Xor6)
    F34 = Flow(Label("Yes"))
    F35 = Flow(Label("No"))
    Xor6.addOutFlow(F34)
    Xor6.addOutFlow(F35)

    #PT EXPIRED1
    End4 = EndNode(Label("pt expired 1"))
    F34.setToNode(End4)

    #XOR7
    Xor7 = XorNode(Label("Xor7 examTime = now"))
    F35.setToNode(Xor7)
    F36 = Flow(Label("No - examTime != now"))
    F37 = Flow(Label("Yes - examTime == now"))
    Xor7.addOutFlow(F36)
    Xor7.addOutFlow(F37)

    #INTERMEDIATE4
    I4 = MsgIntermediateNode(Label("Seconds throw pt"))
    F37.setToNode(I4)
    F38 = Flow(Label("N/A"))
    I4.addOutFlow(F38)
    I4.addOutMsg(M1)

    #END5
    End5 = EndNode(Label("End196"))
    F38.setToNode(End5)

    #XOR7 REPEAT DESTINATIONS
    F36.setToNode(A5)

    #### END THIRD PROCESS (PATIENT-CAREGIVER) ####

    #### START MACROS ####
    Macro1 = Macro("SeverityLevelHigh", "sevLvl>2")
    Macro2 = Macro("SeverityLevelLow", "sevLvl<=2")
    #TODO figure out discharge criteria
    Macro3 = Macro("DischargeCriteriaMet", "True")




if __name__ == "__main__":
    main()
