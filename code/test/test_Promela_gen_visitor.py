from BPMN.BPMN import *
from PromelaGeneration.Promela_gen_visitor import Promela_gen_visitor
import csv
import xml.etree.ElementTree as ET

def parseXML(xmlFile):
    tree = ET.parse(xmlFile)
    return tree

if __name__ == "__main__":
    tree = parseXML("./../assets/online-shopping-model.bpmn")
    print(ET.dump(tree))
