# Summary

Date : 2023-06-14 14:20:05

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration

Total : 3 files,  575 codes, 60 comments, 91 blanks, all 726 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 3 | 575 | 60 | 91 | 726 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 3 | 575 | 60 | 91 | 726 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)