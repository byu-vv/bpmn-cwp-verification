# Diff Summary

Date : 2023-06-14 14:20:05

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration

Total : 22 files,  -1791 codes, -138 comments, -266 blanks, all -2195 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Shell Script | 3 | -87 | -4 | -11 | -102 |
| Python | 19 | -1,704 | -134 | -255 | -2,093 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 22 | -1,791 | -138 | -266 | -2,195 |
| .. | 22 | -1,791 | -138 | -266 | -2,195 |
| .. (Files) | 3 | -209 | -13 | -28 | -250 |
| ../BPMN | 1 | -173 | -4 | -42 | -219 |
| ../BPMN-Generate | 2 | -252 | -59 | -43 | -354 |
| ../BPMN_Visitor | 1 | -38 | 0 | -2 | -40 |
| ../CSVIngest | 1 | -66 | -9 | -10 | -85 |
| ../CWP | 1 | -46 | -8 | -15 | -69 |
| ../CWP-Generate | 2 | -25 | 0 | -7 | -32 |
| ../CounterExampleParser | 1 | -124 | 0 | -12 | -136 |
| ../CounterExampleVisualize | 3 | -317 | -12 | -29 | -358 |
| ../ExpressionParse | 1 | -141 | -2 | -22 | -165 |
| ../StateIngest | 2 | -50 | -7 | -8 | -65 |
| ../Util | 2 | -59 | -4 | -10 | -73 |
| ../XMLIngest | 2 | -291 | -20 | -38 | -349 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)