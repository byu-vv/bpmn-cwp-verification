# Diff Details

Date : 2023-06-14 14:20:05

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration

Total : 22 files,  -1791 codes, -138 comments, -266 blanks, all -2195 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [code/src/BPMN-Generate/LongScaling.py](/code/src/BPMN-Generate/LongScaling.py) | Python | -136 | -31 | -22 | -189 |
| [code/src/BPMN-Generate/WideScaling.py](/code/src/BPMN-Generate/WideScaling.py) | Python | -116 | -28 | -21 | -165 |
| [code/src/BPMN/BPMN.py](/code/src/BPMN/BPMN.py) | Python | -173 | -4 | -42 | -219 |
| [code/src/BPMN_Visitor/BPMN_Visitor.py](/code/src/BPMN_Visitor/BPMN_Visitor.py) | Python | -38 | 0 | -2 | -40 |
| [code/src/CSVIngest/CSVIngestor.py](/code/src/CSVIngest/CSVIngestor.py) | Python | -66 | -9 | -10 | -85 |
| [code/src/CWP-Generate/LongScaling.py](/code/src/CWP-Generate/LongScaling.py) | Python | 0 | 0 | -1 | -1 |
| [code/src/CWP-Generate/WideScaling.py](/code/src/CWP-Generate/WideScaling.py) | Python | -25 | 0 | -6 | -31 |
| [code/src/CWP/CWP.py](/code/src/CWP/CWP.py) | Python | -46 | -8 | -15 | -69 |
| [code/src/CounterExampleParser/CounterExampleParser.py](/code/src/CounterExampleParser/CounterExampleParser.py) | Python | -124 | 0 | -12 | -136 |
| [code/src/CounterExampleVisualize/BPMNETModifier.py](/code/src/CounterExampleVisualize/BPMNETModifier.py) | Python | -136 | 0 | -11 | -147 |
| [code/src/CounterExampleVisualize/CWPETModifier.py](/code/src/CounterExampleVisualize/CWPETModifier.py) | Python | -59 | 0 | -3 | -62 |
| [code/src/CounterExampleVisualize/CounterExampleXMLGenerator.py](/code/src/CounterExampleVisualize/CounterExampleXMLGenerator.py) | Python | -122 | -12 | -15 | -149 |
| [code/src/ExpressionParse/ExpressionParser.py](/code/src/ExpressionParse/ExpressionParser.py) | Python | -141 | -2 | -22 | -165 |
| [code/src/StateIngest/ActivityModifiesIngest.py](/code/src/StateIngest/ActivityModifiesIngest.py) | Python | -13 | 0 | -1 | -14 |
| [code/src/StateIngest/StateIngestor.py](/code/src/StateIngest/StateIngestor.py) | Python | -37 | -7 | -7 | -51 |
| [code/src/Util/genCounterExampleImage.sh](/code/src/Util/genCounterExampleImage.sh) | Shell Script | -8 | -2 | -4 | -14 |
| [code/src/Util/verify.sh](/code/src/Util/verify.sh) | Shell Script | -51 | -2 | -6 | -59 |
| [code/src/XMLIngest/BPMNXMLIngestor.py](/code/src/XMLIngest/BPMNXMLIngestor.py) | Python | -216 | -11 | -26 | -253 |
| [code/src/XMLIngest/CWPXMLIngestor.py](/code/src/XMLIngest/CWPXMLIngestor.py) | Python | -75 | -9 | -12 | -96 |
| [code/src/__init__.py](/code/src/__init__.py) | Python | 0 | 0 | -1 | -1 |
| [code/src/main.py](/code/src/main.py) | Python | -181 | -13 | -26 | -220 |
| [code/src/run_all_examples.sh](/code/src/run_all_examples.sh) | Shell Script | -28 | 0 | -1 | -29 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details