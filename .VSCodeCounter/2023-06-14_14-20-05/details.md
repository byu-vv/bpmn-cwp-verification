# Details

Date : 2023-06-14 14:20:05

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration

Total : 3 files,  575 codes, 60 comments, 91 blanks, all 726 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [code/src/PromelaGeneration/LTL_gen.py](/code/src/PromelaGeneration/LTL_gen.py) | Python | 223 | 18 | 33 | 274 |
| [code/src/PromelaGeneration/Promela_gen_visitor.py](/code/src/PromelaGeneration/Promela_gen_visitor.py) | Python | 264 | 36 | 36 | 336 |
| [code/src/PromelaGeneration/Stub_gen_visitor.py](/code/src/PromelaGeneration/Stub_gen_visitor.py) | Python | 88 | 6 | 22 | 116 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)