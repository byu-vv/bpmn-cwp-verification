Date : 2023-06-14 10:25:50
Directory : /home/bryce/Research/bpmn-cwp-verification/code/src
Total : 25 files,  2366 codes, 198 comments, 357 blanks, all 2921 lines

Languages
+--------------+------------+------------+------------+------------+------------+
| language     | files      | code       | comment    | blank      | total      |
+--------------+------------+------------+------------+------------+------------+
| Python       |         22 |      2,279 |        194 |        346 |      2,819 |
| Shell Script |          3 |         87 |          4 |         11 |        102 |
+--------------+------------+------------+------------+------------+------------+

Directories
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                      | files      | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                         |         25 |      2,366 |        198 |        357 |      2,921 |
| . (Files)                                                                                                 |          3 |        209 |         13 |         28 |        250 |
| BPMN                                                                                                      |          1 |        173 |          4 |         42 |        219 |
| BPMN-Generate                                                                                             |          2 |        252 |         59 |         43 |        354 |
| BPMN_Visitor                                                                                              |          1 |         38 |          0 |          2 |         40 |
| CSVIngest                                                                                                 |          1 |         66 |          9 |         10 |         85 |
| CWP                                                                                                       |          1 |         46 |          8 |         15 |         69 |
| CWP-Generate                                                                                              |          2 |         25 |          0 |          7 |         32 |
| CounterExampleParser                                                                                      |          1 |        124 |          0 |         12 |        136 |
| CounterExampleVisualize                                                                                   |          3 |        317 |         12 |         29 |        358 |
| ExpressionParse                                                                                           |          1 |        141 |          2 |         22 |        165 |
| PromelaGeneration                                                                                         |          3 |        575 |         60 |         91 |        726 |
| StateIngest                                                                                               |          2 |         50 |          7 |          8 |         65 |
| Util                                                                                                      |          2 |         59 |          4 |         10 |         73 |
| XMLIngest                                                                                                 |          2 |        291 |         20 |         38 |        349 |
+-----------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+-----------------------------------------------------------------------------------------------------------+--------------+------------+------------+------------+------------+
| filename                                                                                                  | language     | code       | comment    | blank      | total      |
+-----------------------------------------------------------------------------------------------------------+--------------+------------+------------+------------+------------+
| /home/bryce/Research/bpmn-cwp-verification/code/src/BPMN-Generate/LongScaling.py                          | Python       |        136 |         31 |         22 |        189 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/BPMN-Generate/WideScaling.py                          | Python       |        116 |         28 |         21 |        165 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/BPMN/BPMN.py                                          | Python       |        173 |          4 |         42 |        219 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/BPMN_Visitor/BPMN_Visitor.py                          | Python       |         38 |          0 |          2 |         40 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CSVIngest/CSVIngestor.py                              | Python       |         66 |          9 |         10 |         85 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CWP-Generate/LongScaling.py                           | Python       |          0 |          0 |          1 |          1 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CWP-Generate/WideScaling.py                           | Python       |         25 |          0 |          6 |         31 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CWP/CWP.py                                            | Python       |         46 |          8 |         15 |         69 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleParser/CounterExampleParser.py          | Python       |        124 |          0 |         12 |        136 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize/BPMNETModifier.py             | Python       |        136 |          0 |         11 |        147 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize/CWPETModifier.py              | Python       |         59 |          0 |          3 |         62 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize/CounterExampleXMLGenerator.py | Python       |        122 |         12 |         15 |        149 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/ExpressionParse/ExpressionParser.py                   | Python       |        141 |          2 |         22 |        165 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration/LTL_gen.py                          | Python       |        223 |         18 |         33 |        274 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration/Promela_gen_visitor.py              | Python       |        264 |         36 |         36 |        336 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/PromelaGeneration/Stub_gen_visitor.py                 | Python       |         88 |          6 |         22 |        116 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/StateIngest/ActivityModifiesIngest.py                 | Python       |         13 |          0 |          1 |         14 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/StateIngest/StateIngestor.py                          | Python       |         37 |          7 |          7 |         51 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/Util/genCounterExampleImage.sh                        | Shell Script |          8 |          2 |          4 |         14 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/Util/verify.sh                                        | Shell Script |         51 |          2 |          6 |         59 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/XMLIngest/BPMNXMLIngestor.py                          | Python       |        216 |         11 |         26 |        253 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/XMLIngest/CWPXMLIngestor.py                           | Python       |         75 |          9 |         12 |         96 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/__init__.py                                           | Python       |          0 |          0 |          1 |          1 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/main.py                                               | Python       |        181 |         13 |         26 |        220 |
| /home/bryce/Research/bpmn-cwp-verification/code/src/run_all_examples.sh                                   | Shell Script |         28 |          0 |          1 |         29 |
| Total                                                                                                     |              |      2,366 |        198 |        357 |      2,921 |
+-----------------------------------------------------------------------------------------------------------+--------------+------------+------------+------------+------------+