# Summary

Date : 2023-06-14 10:25:50

Directory /home/bryce/Research/bpmn-cwp-verification/code/src

Total : 25 files,  2366 codes, 198 comments, 357 blanks, all 2921 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 22 | 2,279 | 194 | 346 | 2,819 |
| Shell Script | 3 | 87 | 4 | 11 | 102 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 25 | 2,366 | 198 | 357 | 2,921 |
| . (Files) | 3 | 209 | 13 | 28 | 250 |
| BPMN | 1 | 173 | 4 | 42 | 219 |
| BPMN-Generate | 2 | 252 | 59 | 43 | 354 |
| BPMN_Visitor | 1 | 38 | 0 | 2 | 40 |
| CSVIngest | 1 | 66 | 9 | 10 | 85 |
| CWP | 1 | 46 | 8 | 15 | 69 |
| CWP-Generate | 2 | 25 | 0 | 7 | 32 |
| CounterExampleParser | 1 | 124 | 0 | 12 | 136 |
| CounterExampleVisualize | 3 | 317 | 12 | 29 | 358 |
| ExpressionParse | 1 | 141 | 2 | 22 | 165 |
| PromelaGeneration | 3 | 575 | 60 | 91 | 726 |
| StateIngest | 2 | 50 | 7 | 8 | 65 |
| Util | 2 | 59 | 4 | 10 | 73 |
| XMLIngest | 2 | 291 | 20 | 38 | 349 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)