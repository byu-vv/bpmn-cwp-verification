# Diff Summary

Date : 2023-06-14 15:24:56

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize

Total : 182 files,  -17994 codes, -1132 comments, -1241 blanks, all -20367 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| pip requirements | 1 | 0 | 0 | -1 | -1 |
| Markdown | 1 | -15 | 0 | -5 | -20 |
| Shell Script | 3 | -87 | -4 | -11 | -102 |
| CSV | 6 | -96 | 0 | 0 | -96 |
| LaTeX | 1 | -271 | -17 | -109 | -397 |
| BibTeX | 2 | -332 | 0 | -43 | -375 |
| Python | 25 | -2,140 | -234 | -383 | -2,757 |
| Promela | 37 | -3,822 | -856 | -655 | -5,333 |
| XML | 106 | -11,231 | -21 | -34 | -11,286 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 182 | -17,994 | -1,132 | -1,241 | -20,367 |
| .. | 182 | -17,994 | -1,132 | -1,241 | -20,367 |
| .. (Files) | 3 | -209 | -13 | -28 | -250 |
| ../.. | 160 | -15,945 | -946 | -912 | -17,803 |
| ../.. (Files) | 1 | 0 | 0 | -1 | -1 |
| ../../.. | 38 | -4,734 | -34 | -172 | -4,940 |
| ../../.. (Files) | 6 | -837 | -2 | -8 | -847 |
| ../../../Papers | 29 | -3,888 | -29 | -164 | -4,081 |
| ../../../Papers (Files) | 3 | -603 | -17 | -152 | -772 |
| ../../../Papers/assets | 3 | -246 | -1 | 0 | -247 |
| ../../../Papers/thesis | 23 | -3,039 | -11 | -12 | -3,062 |
| ../../../Presentations | 3 | -9 | -3 | 0 | -12 |
| ../../assets | 116 | -11,035 | -860 | -675 | -12,570 |
| ../../assets/examples | 114 | -10,925 | -860 | -673 | -12,458 |
| ../../assets/examples (Files) | 1 | -76 | 0 | -1 | -77 |
| ../../assets/examples/buynsell_Dec_20_2022 | 3 | -515 | -24 | -5 | -544 |
| ../../assets/examples/buynsell_Mar_27_2023 | 4 | -786 | -31 | -8 | -825 |
| ../../assets/examples/debug | 3 | -90 | -1 | -1 | -92 |
| ../../assets/examples/face2face_Jan_6_2023 | 5 | -569 | -19 | -6 | -594 |
| ../../assets/examples/face2face_Mar_27_2023 | 3 | -495 | -18 | -5 | -518 |
| ../../assets/examples/face2face_May_5_2023 | 3 | -455 | -17 | -4 | -476 |
| ../../assets/examples/face2face_May_5_2023_error | 3 | -404 | -14 | -4 | -422 |
| ../../assets/examples/longScaling1 | 3 | -127 | -5 | -3 | -135 |
| ../../assets/examples/longScaling10 | 3 | -217 | -23 | -21 | -261 |
| ../../assets/examples/longScaling11 | 3 | -227 | -25 | -23 | -275 |
| ../../assets/examples/longScaling12 | 3 | -237 | -27 | -25 | -289 |
| ../../assets/examples/longScaling13 | 3 | -247 | -29 | -27 | -303 |
| ../../assets/examples/longScaling14 | 3 | -257 | -31 | -29 | -317 |
| ../../assets/examples/longScaling15 | 3 | -267 | -33 | -31 | -331 |
| ../../assets/examples/longScaling16 | 3 | -277 | -35 | -33 | -345 |
| ../../assets/examples/longScaling17 | 3 | -287 | -37 | -35 | -359 |
| ../../assets/examples/longScaling18 | 3 | -297 | -39 | -37 | -373 |
| ../../assets/examples/longScaling19 | 3 | -307 | -41 | -39 | -387 |
| ../../assets/examples/longScaling2 | 3 | -137 | -7 | -5 | -149 |
| ../../assets/examples/longScaling20 | 3 | -317 | -43 | -41 | -401 |
| ../../assets/examples/longScaling3 | 3 | -147 | -9 | -7 | -163 |
| ../../assets/examples/longScaling4 | 3 | -157 | -11 | -9 | -177 |
| ../../assets/examples/longScaling5 | 3 | -167 | -13 | -11 | -191 |
| ../../assets/examples/longScaling6 | 3 | -177 | -15 | -13 | -205 |
| ../../assets/examples/longScaling7 | 3 | -187 | -17 | -15 | -219 |
| ../../assets/examples/longScaling8 | 3 | -197 | -19 | -17 | -233 |
| ../../assets/examples/longScaling9 | 3 | -207 | -21 | -19 | -247 |
| ../../assets/examples/onlineShopping_Dec_20_2022 | 2 | -257 | 0 | 0 | -257 |
| ../../assets/examples/phware_Dec_20_2022 | 3 | -815 | -32 | -3 | -850 |
| ../../assets/examples/phware_May_5_2023 | 5 | -953 | -33 | -4 | -990 |
| ../../assets/examples/simplePurchase_Dec_20_2022 | 3 | -266 | -11 | -4 | -281 |
| ../../assets/examples/wideScaling1 | 2 | -41 | -5 | -6 | -52 |
| ../../assets/examples/wideScaling2 | 2 | -58 | -10 | -11 | -79 |
| ../../assets/examples/wideScaling3 | 2 | -75 | -15 | -16 | -106 |
| ../../assets/examples/wideScaling4 | 2 | -92 | -20 | -21 | -133 |
| ../../assets/examples/wideScaling5 | 2 | -109 | -25 | -26 | -160 |
| ../../assets/examples/wideScaling6 | 2 | -126 | -30 | -31 | -187 |
| ../../assets/examples/wideScaling7 | 2 | -143 | -35 | -36 | -214 |
| ../../assets/examples/wideScaling8 | 2 | -160 | -40 | -41 | -241 |
| ../../assets/stubs | 2 | -110 | 0 | -2 | -112 |
| ../../test | 5 | -176 | -52 | -64 | -292 |
| ../BPMN | 1 | -173 | -4 | -43 | -220 |
| ../BPMN-Generate | 2 | -252 | -59 | -43 | -354 |
| ../BPMN_Visitor | 1 | -38 | 0 | -2 | -40 |
| ../CSVIngest | 1 | -66 | -9 | -10 | -85 |
| ../CWP | 1 | -46 | -8 | -15 | -69 |
| ../CWP-Generate | 2 | -25 | 0 | -7 | -32 |
| ../CounterExampleParser | 1 | -124 | 0 | -12 | -136 |
| ../ExpressionParse | 1 | -141 | -2 | -22 | -165 |
| ../PromelaGeneration | 3 | -575 | -60 | -91 | -726 |
| ../StateIngest | 2 | -50 | -7 | -8 | -65 |
| ../Util | 2 | -59 | -4 | -10 | -73 |
| ../XMLIngest | 2 | -291 | -20 | -38 | -349 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)