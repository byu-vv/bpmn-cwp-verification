# Summary

Date : 2023-06-14 15:24:56

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize

Total : 3 files,  317 codes, 12 comments, 29 blanks, all 358 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 3 | 317 | 12 | 29 | 358 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 3 | 317 | 12 | 29 | 358 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)