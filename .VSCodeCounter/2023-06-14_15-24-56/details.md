# Details

Date : 2023-06-14 15:24:56

Directory /home/bryce/Research/bpmn-cwp-verification/code/src/CounterExampleVisualize

Total : 3 files,  317 codes, 12 comments, 29 blanks, all 358 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [code/src/CounterExampleVisualize/BPMNETModifier.py](/code/src/CounterExampleVisualize/BPMNETModifier.py) | Python | 136 | 0 | 11 | 147 |
| [code/src/CounterExampleVisualize/CWPETModifier.py](/code/src/CounterExampleVisualize/CWPETModifier.py) | Python | 59 | 0 | 3 | 62 |
| [code/src/CounterExampleVisualize/CounterExampleXMLGenerator.py](/code/src/CounterExampleVisualize/CounterExampleXMLGenerator.py) | Python | 122 | 12 | 15 | 149 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)