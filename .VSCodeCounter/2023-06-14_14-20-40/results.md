# Summary

Date : 2023-06-14 14:20:40

Directory /home/bryce/Research/bpmn-cwp-verification

Total : 185 files,  18311 codes, 1144 comments, 1270 blanks, all 20725 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| XML | 106 | 11,231 | 21 | 34 | 11,286 |
| Promela | 37 | 3,822 | 856 | 655 | 5,333 |
| Python | 28 | 2,457 | 246 | 412 | 3,115 |
| BibTeX | 2 | 332 | 0 | 43 | 375 |
| LaTeX | 1 | 271 | 17 | 109 | 397 |
| CSV | 6 | 96 | 0 | 0 | 96 |
| Shell Script | 3 | 87 | 4 | 11 | 102 |
| Markdown | 1 | 15 | 0 | 5 | 20 |
| pip requirements | 1 | 0 | 0 | 1 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 185 | 18,311 | 1,144 | 1,270 | 20,725 |
| . (Files) | 6 | 837 | 2 | 8 | 847 |
| Papers | 29 | 3,888 | 29 | 164 | 4,081 |
| Papers (Files) | 3 | 603 | 17 | 152 | 772 |
| Papers/assets | 3 | 246 | 1 | 0 | 247 |
| Papers/thesis | 23 | 3,039 | 11 | 12 | 3,062 |
| Presentations | 3 | 9 | 3 | 0 | 12 |
| code | 147 | 13,577 | 1,110 | 1,098 | 15,785 |
| code (Files) | 1 | 0 | 0 | 1 | 1 |
| code/assets | 116 | 11,035 | 860 | 675 | 12,570 |
| code/assets/examples | 114 | 10,925 | 860 | 673 | 12,458 |
| code/assets/examples (Files) | 1 | 76 | 0 | 1 | 77 |
| code/assets/examples/buynsell_Dec_20_2022 | 3 | 515 | 24 | 5 | 544 |
| code/assets/examples/buynsell_Mar_27_2023 | 4 | 786 | 31 | 8 | 825 |
| code/assets/examples/debug | 3 | 90 | 1 | 1 | 92 |
| code/assets/examples/face2face_Jan_6_2023 | 5 | 569 | 19 | 6 | 594 |
| code/assets/examples/face2face_Mar_27_2023 | 3 | 495 | 18 | 5 | 518 |
| code/assets/examples/face2face_May_5_2023 | 3 | 455 | 17 | 4 | 476 |
| code/assets/examples/face2face_May_5_2023_error | 3 | 404 | 14 | 4 | 422 |
| code/assets/examples/longScaling1 | 3 | 127 | 5 | 3 | 135 |
| code/assets/examples/longScaling10 | 3 | 217 | 23 | 21 | 261 |
| code/assets/examples/longScaling11 | 3 | 227 | 25 | 23 | 275 |
| code/assets/examples/longScaling12 | 3 | 237 | 27 | 25 | 289 |
| code/assets/examples/longScaling13 | 3 | 247 | 29 | 27 | 303 |
| code/assets/examples/longScaling14 | 3 | 257 | 31 | 29 | 317 |
| code/assets/examples/longScaling15 | 3 | 267 | 33 | 31 | 331 |
| code/assets/examples/longScaling16 | 3 | 277 | 35 | 33 | 345 |
| code/assets/examples/longScaling17 | 3 | 287 | 37 | 35 | 359 |
| code/assets/examples/longScaling18 | 3 | 297 | 39 | 37 | 373 |
| code/assets/examples/longScaling19 | 3 | 307 | 41 | 39 | 387 |
| code/assets/examples/longScaling2 | 3 | 137 | 7 | 5 | 149 |
| code/assets/examples/longScaling20 | 3 | 317 | 43 | 41 | 401 |
| code/assets/examples/longScaling3 | 3 | 147 | 9 | 7 | 163 |
| code/assets/examples/longScaling4 | 3 | 157 | 11 | 9 | 177 |
| code/assets/examples/longScaling5 | 3 | 167 | 13 | 11 | 191 |
| code/assets/examples/longScaling6 | 3 | 177 | 15 | 13 | 205 |
| code/assets/examples/longScaling7 | 3 | 187 | 17 | 15 | 219 |
| code/assets/examples/longScaling8 | 3 | 197 | 19 | 17 | 233 |
| code/assets/examples/longScaling9 | 3 | 207 | 21 | 19 | 247 |
| code/assets/examples/onlineShopping_Dec_20_2022 | 2 | 257 | 0 | 0 | 257 |
| code/assets/examples/phware_Dec_20_2022 | 3 | 815 | 32 | 3 | 850 |
| code/assets/examples/phware_May_5_2023 | 5 | 953 | 33 | 4 | 990 |
| code/assets/examples/simplePurchase_Dec_20_2022 | 3 | 266 | 11 | 4 | 281 |
| code/assets/examples/wideScaling1 | 2 | 41 | 5 | 6 | 52 |
| code/assets/examples/wideScaling2 | 2 | 58 | 10 | 11 | 79 |
| code/assets/examples/wideScaling3 | 2 | 75 | 15 | 16 | 106 |
| code/assets/examples/wideScaling4 | 2 | 92 | 20 | 21 | 133 |
| code/assets/examples/wideScaling5 | 2 | 109 | 25 | 26 | 160 |
| code/assets/examples/wideScaling6 | 2 | 126 | 30 | 31 | 187 |
| code/assets/examples/wideScaling7 | 2 | 143 | 35 | 36 | 214 |
| code/assets/examples/wideScaling8 | 2 | 160 | 40 | 41 | 241 |
| code/assets/stubs | 2 | 110 | 0 | 2 | 112 |
| code/src | 25 | 2,366 | 198 | 358 | 2,922 |
| code/src (Files) | 3 | 209 | 13 | 28 | 250 |
| code/src/BPMN | 1 | 173 | 4 | 43 | 220 |
| code/src/BPMN-Generate | 2 | 252 | 59 | 43 | 354 |
| code/src/BPMN_Visitor | 1 | 38 | 0 | 2 | 40 |
| code/src/CSVIngest | 1 | 66 | 9 | 10 | 85 |
| code/src/CWP | 1 | 46 | 8 | 15 | 69 |
| code/src/CWP-Generate | 2 | 25 | 0 | 7 | 32 |
| code/src/CounterExampleParser | 1 | 124 | 0 | 12 | 136 |
| code/src/CounterExampleVisualize | 3 | 317 | 12 | 29 | 358 |
| code/src/ExpressionParse | 1 | 141 | 2 | 22 | 165 |
| code/src/PromelaGeneration | 3 | 575 | 60 | 91 | 726 |
| code/src/StateIngest | 2 | 50 | 7 | 8 | 65 |
| code/src/Util | 2 | 59 | 4 | 10 | 73 |
| code/src/XMLIngest | 2 | 291 | 20 | 38 | 349 |
| code/test | 5 | 176 | 52 | 64 | 292 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)