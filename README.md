for verification:

pip install numpy
pip install pandas
change projectDir in verify.sh
install spin (sudo apt install spin)

For counterexample generation:

sudo apt install npm
sudo npm install -g bpmn-to-image
install nvm (curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash)
sudo apt install libnss3
sudo apt install libgbm1
sudo apt install libasound2
nvm install 16
nvm use 16


docker run -it -e DRAWIO_DESKTOP_COMMAND_TIMEOUT='10000s' -w /data -v .:/data rlespinasse/drawio-desktop-headless -x -f png ./"