\documentclass[11pt]{article}

\title{Automating Model Checking of Human and Machine Integration}

\author{Bryce Pierson}

\date{Feb. 10, 2022}

\pagenumbering{arabic}

\bibliographystyle{plain-annote}


\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{float}
\usepackage{comment}
\usepackage{algorithmic}
\usepackage{amsmath,amssymb,amsfonts}


\newcommand{\figref}[1]{Fig.~\ref{#1}}
\newcommand{\secref}[1]{Sec.~\ref{#1}}

\lstdefinestyle{myPromela}
{frame=none,
  basicstyle=\ttfamily,
  language=Promela,
  aboveskip=1mm,
  belowskip=1mm,
  showstringspaces=false,
  columns=flexible,
  numbers=none,
  numberstyle=\tiny\color{gray},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=false,
  breakatwhitespace=true,
  tabsize=2,
  linewidth=2\linewidth,
  morekeywords={always, eventually, until, implies, ltl}
}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}
\begin{document}
\begin{titlepage}
\maketitle
\thispagestyle{empty}
\end{titlepage}
\pagebreak
\section*{Abstract}
A business process model (BPMN) is a tool used to model complex interactions between humans and machines in large systems. There are nuances in these models including asynchrony and performance differences between humans and machines. These nuances are often difficult to reason about manually. A Cognitive Work Problem (CWP) can be used to provide fundamental requirements for both what a BPMN should provide and also restrictions on how it can accomplish those requirements. Currently, it is difficult to know if a complex BPMN correctly implements a CWP.  This paper details the work to translate BPMNs into Process Meta Language (ProMeLa) code and CWPs into LTL properties.  It also shows that model checkers such as SPIN can use the generated ProMeLa and LTL to prove absence of invalid transitions and end states. This allows anyone who can write BPMNs and CWPs, which are both graphical languages, the ability to prove their BPMN holds under the constraints set forth by the CWP. This research also explores the modifications and assumptions made about the BPMNs and CWPs during translation into ProMeLa and how they may affect the users' trust in the proof.
\pagebreak

\tableofcontents
\pagebreak

\section{Introduction}
Modern businesses clearly define operations of both human and machine actors using Business Process Models (BPMNs). Formal verification for these modeled systems of business operations is an ongoing field of research. Limited research has been done on automatic verification of business process models against static properties. There is need for an automated tool that verifies business process models against given properties. This differs from existing research on BPMN analysis because it verifies models using given properties, instead of using static properties such as avoidance of deadlock. Prior research may have avoided handling arbitrarily complex given properties because it can greatly contribute to both time and space complexity of a comprehensive proof.

In this research, BPMNs will be translated into Process Meta Language (ProMeLa) to facilitate model checking. ProMeLa is a language that allows for both nondeterminism and concurrency, which are crucial to understanding the nuanced asynchrony of the system in question. Petri Nets have been used in prior BPMN validation research, and it will be used here as well. Petri nets are directed bipartite graphs that have an exact mathematical definition to their execution semantics. In short, tokens reside in places in the Petri Net, and those tokens can trigger transitions, which consume and produce tokens in specified places. BPMN semantics align closely with Petri Net semantics since both use a token activation mechanism.

Cognitive Wor Problems (CWPs) are diagrams capturing the fundamental requirements of conceptual work. They are used in this research to provide a set of behavior restraints on the BPMNs. CWPs are used here because they bridge the gap between human and computing resources by making the work conceptual. This means that any BPMN that aims to implement a particular CWP can do so using any tools available, regardless of functional performance differences. For example, a CWP defining expected behavior of a store can be implemented by a BPMN describing the operations of an online store, a restaurant, or a garage sale.

In this research, CWPs will be translated into properties in Linear Temporal Logic (LTL). LTL is a first order predicate calculus with the capability of reasoning about truth values in a system over time. LTL can be used in model checking as a backdrop of properties a particular model must adhere to. In this research, model checking is referring to the practice of combining a finite-state model with temporal properties to prove the absence or existence of undesired potential execution behavior of the model. SPIN is the model checker used in this research.

This research looks into ways to optimize verification time and curb verification state space explosion. It is anticipated that CWPs can be used to provide logical restrictions on behavior of BPMNs. I propose that the automated translation of business process models into Petri Net ProMeLa models, the automated translation of CWPs into sets of LTL properties, and the automated verification of the Petri Net models in respect to the  LTL properties can all be performed within an hour on everyday hardware.

\section{Background}
\subsection{Business Process Models}
BPMNs are widely used by organizations to depict human and machine operations \cite{Dijkman11}. Many international companies maintain repositories of hundreds or even thousands of BPMNs in an effort to standardize and incrementally improve their operations \cite{Rosemann06}.  There has been some work on the verification of business processes \cite{watahiki11} but existing research verifies primarily that business process models satisfy certain constant time and resource constraints (finish within a certain amount of time, etc).

One of the most common standards for business process models is Business Process Modeling Notation (BPMN). BPMN will be used throughout this paper as the input for automatic generation of verifiable state models \cite{white08}. The following paragraph will describe the basics of BPMN notation, and an example of BPMN notation can be found in \figref{fig:BPMN-Example}.

The example BPMN in \figref{fig:BPMN-Example} shows a simplified process of online shopping. Each large rectangle is a BPMN pool, each representing a different actor in the process. The top pool represents an online shopper while the bottom one represents the store. Each pool contains a flow diagram representing an actor participating in the process. The hollow circles on the left indicate start events where the flow begins. Smaller, rounded rectangles are activities such as ``log in to website'' and ``place order''. Long dashed lines represent messages passed between pools. The dashed line between the ``place order'' activity and the ``receive order'' start event tell us that when the shopper places an order, it triggers the store to begin processing that order. Short dashed lines indicate communication with some sort of database, such as the store inventory. Diamond boxes are branching points in the model. The store, for example, behaves differently if the item is in stock or not. In our case, the branch is an ``exclusive or'' branch, meaning that exactly one of the paths will be taken. This is indicated by the X in the diamond.

The goal in this research is to verify that certain properties are upheld by a given BPMN. We are choosing to accept properties written as CWPs.

\begin{figure}
  \centering
  
  \includegraphics[width=\linewidth]{assets/online-shopping-model.png}
  
  \caption{BPMN Example - Online Shopping}
  \label{fig:BPMN-Example}
  \end{figure}
  
  \begin{figure}
  \centering
  
  \includegraphics[width=\linewidth, keepaspectratio]{assets/Customer CWP.pdf}
  
  \caption{CWP Example - Online Shopping}
  \label{fig:CWP-Example}
  \end{figure}
  
\begin{comment}
BPMN notation consists of events, activities, gateways, and flows. Events represent triggers that start, modify, or complete processes. They are shown as circles. Activities represent tasks performed by a person or machine. It is important that, when interpreting the process as a state machine, most of the changes to the state happen during activities. They are where the action happens within a process model. Activities are shown as rounded rectangles. Gateways are decision points that adjust the flow based on conditions or events. They are shown as diamonds. Flows show the order of activities and events. They are shown as straight lines with arrows.  Dashed lines indicate sent messages between activities and events. Databases can also be included in a model, even when the flow doesn't pass through them. Data traffic to and from databases is indicated by dotted lines. BPMN also contains a hierarchical structure for participants in a process. A pool contains all major participants in a process, while swim lanes within a pool define who is responsible for what parts of the process.
\end{comment}

\subsection{Cognitive Work Problems}

Cognitive Work Problems (CWPs) are UML-like diagrams capturing the fundamental requirements for conceptual work. They provide an explicit specification for the information product a workflow must produce. It provides a shareable object of abstract work that can be provided to any actor, regardless of performance properties. Thus, CWPs provide the means to integrate the work of a distributed cognitive system. Here, we reuse CWPs as a property to be modeled by a specific system, given through a BPMN. An example of a CWP is given in \figref{fig:CWP-Example}.

A CWP can be broken down into two sections: 1) a state description often in the form of a UML class diagram and 2) a UML-like state diagram defining paths from the initial state to the end states. There must be a connection between the BPMN state variables and the variables in the CWP state description in order for the verification between a BPMN and a CWP to take place. Nodes in a CWP can represent states an implementing model is allowed to reside in. Edge labels contain atomic propositions that assist in uniquely identifying the system state.

While a BPMN defines how a system does something, a CWP defines what is being done. For example, the CWP in \figref{fig:CWP-Example} is specifying certain desired behaviors of the BPMN in \figref{fig:BPMN-Example}. For example, \figref{fig:CWP-Example} requires a transaction to begin with an order being submitted and end with either the order being canceled or the customer receiving the order. Having any other start or end states is a violation of the property represented by the CWP. A CWP defines a set of rules that must be followed by any BPMN which claims to implement it. The only BPMN behavior we care about in this research is behavior that is defined by a CWP it attests to implement.

The CWP in \figref{fig:CWP-Example} is just one example of a CWP that the BPMN in \figref{fig:BPMN-Example} implements. This particular CWP is concerned primarily with the correct behavior in regards to inventory availability and payment reception. We could also draft CWPs that outline properties about customer notifications or discount calculations. In order to remain as simple and intuitive as possible, \figref{fig:CWP-Example} was chosen as the example CWP.

\subsection{Model Checking}
Model checking is a method for verifying whether a finite-state model of a system satisfies a given specification. If a model behaves as expected during evaluation with each property in the specification, the model can be considered correct. The model checker used in this research is SPIN. SPIN works by translating the model into a finite automaton and the logic properties into buchi automata. It then performs a synchronous product of the automata, failing if an acceptance cycle is found and succeeding if the entire intersection is computed without an acceptance cycle \cite{holz97}.

ProMeLa is a verification modeling language. It allows for dynamic creation of concurrent processes in a single model, for which SPIN can then generate an exhaustive state space. ProMeLa also has support for a couple ways of expressing properties of the model. One of these representations is LTL. ProMeLa can also express properties as never claims, which step in sync with the model and are often used to express properties by mimicing simple finite automata.

\section{Problem Statement and Thesis}

There is a lack of tools for translation from human-readable business process models with accompanying conceptual models to be machine-readable. Additionally, there is no tool to help prove that a BPMN implements a CWP. We want to generate verifiable models which precisely represent the intended meaning behind BPMs. We want to find a way to disambiguate all parts of a BPMN in the process. We also want to examine the differences in various approaches to translate CWPs into temporal properties about the model. Referring to the example given in \figref{fig:BPMN-Example} and \figref{fig:CWP-Example}, we are trying to prove that the process integrating human and machine actors in an online shopping system conforms to the declarative specification of the CWP defining a shopping transaction.

A BPMN's correctness relative to a given CWP can be verified in a reasonable amount of time using a model checker such as SPIN by translating the BPMN to ProMeLa and representing the CWP as a set of LTL properties. This process can be automated despite the frequent ambiguity present in many BPMNs.

\section{Approach}
There are a few pieces to this research project.  There are two major sections, the engineering and the research. Once the engineering is done, I will use the built system to ask questions about the methods and tools in use. 

For the engineering part, the first step is building the code generator, taking BPMN and CWP exports as input and producing a ProMeLa model and a set of LTL properties as output. This step is mostly automating a process performed manually in a paper recently published \cite{mercer22}

In step two, the generated model needs to be verified against the generated LTL properties.  In order to do this, there needs to be a mapping from BPMN model state to CWP states. It is unlikely that this mapping will be able to be automated. The BPMN's author (or some other person closely familiar with it) may need to add some necessary annotations to enable an accurate mapping. This research aims to minimize the required number of annotations. As part of the step two, the generation and validation processes may be automated together such that many models will be generated and evaluated before one is chosen as the "correct" model. Evaluation metrics are given in a later section.


\subsection{BPMN to ProMeLa}
A BPMN model is converted into an intermediate python representation before constructing a ProMeLa model. The ProMeLa model generated will function, similar to other model checking projects, like a simple Petri Net, where each activity in the business process is a node in the Petri Net \cite{Kosch05}. A Petri Net functions by node activation. A node can only activate if it contains a token. During activation, it will consume the token and then generate a token in the next activity (effectively passing it along). For example, the following represent the ``Item in Stock?'' gateway in \figref{fig:BPMN-Example}:

%
{\small
\begin{lstlisting}[style=myPromela]
:: hasToken(ItemInStockGateway) -> atomic { 
    consumeToken(ItemInStockGateway)
    if
    :: isInStock(item) -> 
        putToken(notifyCustomerItemIsUnavailableActivity)
    :: !isInStock(item) -> 
        putToken(CalculateDiscountActivity)
    fi}
\end{lstlisting}
}
% 

The ``::'' at the beginning indicates that this option is one of many non-deterministic options the Petri Net can choose. We call this the event loop. If multiple transitions are available, spin will handle the nondeterminism by exploring both paths fully during verification.

Each swimlane in the BPMN will have its own process and event loop, with an option for each activity, event, or gateway. The process has the following pattern:

\begin{lstlisting}[style=myPromela]
active proctype name() {
  /* Add tokens to initial elements */
  do
  :: hasToken(element_0) ->
    /* element_0 actions */
  :: hasToken(element_1) ->
    /* element_1 actions */
  /* ... */  
  :: hasToken(element_n) ->
    /* element_n actions */
  od
}
\end{lstlisting}

\noindent The do-statement repeats indefinitely. If multiple elements have tokens, then SPIN will choose one to execute nondeterministically. Each of the activities, events, and gateways in the BPMN must have, at a minimum, a ProMeLa variable to represent the presence or absence of a token.

There are plenty of assumptions that are made when interpreting a BPMN and generating ProMeLa code. One example is state variable volatility. Referring to  \figref{fig:BPMN-Example}, can item availability change while the customer's credit card is being charged? Intuitively, the item is set aside for the customer immediately, but this is not fully enumerated in the model.  This means that at each activity between the ''item in stock'', the model must decide whether these variables can freely change values, there is some set of allowed values, or they are fixed. The information regarding how product availability can change within these activities must be inferred during code generation. Not knowing how state variables are allowed to change within activities makes automated ProMeLa generation difficult.

The entire environment, including state variable and inputs, needs to be modeled. State variables, or propositions in the BPMN that affect decisions, must also be modeled in ProMeLa. In the example given, ``isInStock(item)'' represents a state variable for whether or not the item in question is in stock. We anticipate that based on the thoroughness of the BPMN model, some of these state variables will be explicitly stated in the original BPMN and some may need to be added through annotations. Additionally, a mapping between CWP state variables and BPMN state variables needs to be specified and implemented. Inputs, such as orders in the shopping example, also must be implemented and modeled. Each BPMN input and state variable must have a complete set of valid values in order for model checking to be possible.

Because model checking involves a complete state space search, it requires complete knowledge about the environment. This is generally omitted from business process models for readability purposes. Therefore, there must be assumptions made about the model in order to enable verification. In making these assumptions, it is possible that the ProMeLa model no longer represents the intended meaning of the BPMN. Therefore, this work is intended to find the least restrictive interpretation of a BPMN possible that can pass verification. That is, updates on the state and inputs should be minimally constricted. 

In existing work done on verification of BPMNs using model checking, assumptions were answered directly by the author of the BPMN, who understood the process well enough to manually fix the incorrect assumptions. Since this project is attempting to automate this process, there should be some deterministic way of disambiguating the BPMN. One potential solution is to generate an underspecified model and attempt to verify. In this case, an underspecified model means that the set of constraints on environment changes is minimized. If it does not verify, incrementally strengthen the constraints until it does.  Once it verifies, the resulting model could be reviewed by the author for correct assumptions.

\subsection{CWP to LTL}
Finally, the CWP is translated into a set of LTL properties. Many of the properties of real life systems depend on a temporal constraint, such as ``The customer only receives an order after they have paid.'' Using a temporal logic enables verification of these properties. The workflow state of the ProMeLa model depends on a set of atomic propositions. They are defined to indicate when a BPMN state resides in any particular CWP state. Each is computed from incoming ($I$) and outgoing ($O$) transitions on each state as $(\bigwedge_{i \in I}\ i) \wedge \neg(\bigvee_{o \in O}\ o)$: for a state, all the conditions on the incoming transitions hold and none of the conditions on the outgoing transitions hold. These atomic propositions must be mutually exclusive, as defined later, to pass verification.

For example, in \figref{fig:CWP-Example}, the ``Charge Customer'' state can be defined as follows. The conjunction of the incoming transitions (only one transition in this case):
%
\[
  \mathtt{inventoryChecked} = \mathtt{True} \wedge \mathtt{orderInStock} = \mathtt{True}
\]
%

\noindent Conjuncted with the negated disjunction of the outgoing transitions:

%
\[
\begin{array}{c}
  \neg((\mathtt{paymentReceived} = \mathtt{True} \wedge \mathtt{timeExpired} \neq \mathtt{True}) \vee \\
  (\mathtt{paymentReceived} \neq \mathtt{True} \wedge \mathtt{timeExpired} = \mathtt{True}))
\end{array}
\]
%

There are 5 types of LTL properties generated for each CWP. Two are global properties and three apply to individual states. The first LTL property for the CWP is that its states must be universally inclusive meaning that the BPMN state must reside, always, in some CWP state. It is simply an invariant over the disjunction of CWP states:
%
{\small
\begin{lstlisting}[style=myPromela,mathescape=true]
always(   InitState $\vee$ orderSubmittedState 
        $\vee$ inventoryCheckState 
        $\vee$ orderCancelledState 
        $\vee$ chargeCustomerState
        $\vee$ orderFulfilledState
        $\vee$ customerReceivesOrderState
        )
\end{lstlisting}
}
%


The second LTL property is that the BPMN state is able to reach either of the two terminal states in the CWP: \emph{Order Canceled} or \emph{Customer Receives Order}.
These are states in the CWP with no outgoing transitions.
%
{\small
\begin{lstlisting}[style=myPromela,mathescape=true]
eventually(orderCancelledState $\vee$ customerReceivesOrderState)
\end{lstlisting}
}
%
The property is named \texttt{fair} and is an eventuality that should exist in the workflow. Its existence is verified with \texttt{\textbf{always} $\neg$fair} which witnesses the existence of \texttt{fair} as a counter example to the property. The properties applying to individual states will depend on the \texttt{fair} property. We only care about correct and valid transitions if the path those transitions are following lead to a valid end state. Therefore, we use \texttt{fair} on the left side of an implication for multiple upcoming properties.

Fairness in \figref{fig:BPMN-Example} and \figref{fig:CWP-Example} does not impact evaluation because all paths in both diagrams lead to end states. There are no loops. In more complex models, it is possible that a BPMN might cycle through a set of CWP states infinitely, never reaching an end state. If the behavior is unreasonably unlikely or impossible in practice, these traces are not meant to be analyzed and will be pruned during evaluation using the \texttt{fair} LTL property. An example of this is an infinite loop in a health care treatement plan where the patient is never discharged and also never passes away.

The next set of three properties must hold for each CWP state:
(1) the state is reached by some workflow state;
(2) the workflow state is only ever covered by exactly one CWP state;
and (3) only transitions allowed by the CWP are taken by the workflow state.
These are shown by example with the \emph{Inventory Check State}.

The first property is the existence of the state somewhere. It is essential because of the following paradox centered around implications:

\begin{lstlisting}
PaymentReceived ==> OrderFulfilled
\end{lstlisting}

 It may sound encouraging if this property holds, but it's also possible that payments are never received, which satisfies the property but tells us nothing about the order fulfillment process. For this reason, we need an existential testament for each of our CWP states.
 
%
{\small
\begin{lstlisting}[style=myPromela,mathescape=true]

fair $\implies$ always $\neg$ inventoryCheckState

\end{lstlisting}
}
%

The counter-example is the witness that the workflow state is reachable on some fair path.
Recall that a fair path ends in a terminal state.
The implication is not vacuous because the earlier global property produced a witness to the existence of such a path.
Vacuity is avoided similarly in other properties as seen next.

The second property is the BPMN state may only belong to exactly one CWP state.
%
{\small
\begin{lstlisting}[style=myPromela,mathescape=true]
always( inventoryCheckState $\implies$ 
  ( $\neg$InitState
  $\wedge$ $\neg$orderSubmittedState 
  $\wedge$ inventoryCheckState 
  $\wedge$ $\neg$orderCancelledState 
  $\wedge$ $\neg$chargeCustomerState
  $\wedge$ $\neg$orderFulfilledState
  $\wedge$ $\neg$customerReceivesOrderState))
\end{lstlisting}
}
%
When the system is in the \emph{inventoryCheckState} state, it cannot be in any other CWP state simultaneously. If this property fails, there are two potential remedies. First, additional CWP states may need to be added in order to disambiguate the duplicates. Second, the mapping from the BPMN environment to the CWP states could be altered. Either way, it is critical that the workflow state only ever reside in a single CWP state.

The third property is that only transitions allowed by the CWP are implemented by the workflow.
%
{\small
\begin{lstlisting}[style=myPromela,mathescape=true]
fair $\implies$ 
  always( inventoryCheckState $\implies$ 
    (inventoryCheckState until
      ( orderCancelledState
      $\vee$ chargeCustomerState)))
\end{lstlisting}
}
%
Here the BPMN state must remain \emph{until} it transitions to one of the allowed successor states in the CWP.
In this example those states are either \emph{order canceled} or \emph{charge customer}. Keep in mind again that an earlier global property proved the existence of a fair path, so this property will not be vacuously true.

We believe that these five properties are sufficient for capturing the meaning of the CWP. The properties ensure that a model is always constrained to states specified in the CWP, that there are paths leading to the end states, and that the transitions along those paths are conforming to the transitions enumerated in the CWP. The LTL properties are then verified against the ProMeLa model using the SPIN model checker.

\section{Evaluation}

The goal is to have an end-to-end system that can take as input CWP and BPMN exports and generate corresponding LTL properties and ProMeLa models that follow the pattern outlined in this proposal. If any changes are made to the pattern, we hope to show that the changes do not impact the equivalence between the CWP and LTL as well as between the BPMN and ProMeLa. Inferences about the BPMN and CWP models may have to be made, but these inferences will also be documented and shown to be reasonable. For each LTL property generated, the ProMeLa model should be run. For existential properties, the model should fail, providing a testimony to the existence of the property. For all other properties, the model should pass. In this case, we consider the workflow to be a successful implementation of the CWP.

\subsection{Research Questions}

Some questions that will be asked are: 


\begin{comment}
\begin{enumerate}

\item How many actors can be involved in the system before the state space becomes too large to verify in a reasonable amount of time and space? In the shopping example in \figref{fig:BPMN-Example}, how many orders can be allowed to be simultaneously in flight before model verification becomes too lengthy? Are there ways that we can mitigate these problems by modifying the design of the ProMeLa model?

\item How much manual filtering should be involved in the process of making assumptions about the model?

\item Are there ways that ProMeLa and SPIN's debugger could be altered to accommodate complex expressions within a Petri Net design?

\item Might there be situations where a CWP is capturing multiple disjunctive properties that could be analyzed separately to decrease verification time? How could such a situation be determined? 

\end{enumerate}
\end{comment}


\begin{enumerate}

\item In what ways might ProMeLa and SPIN be modified to allow for counter-example generation that can be easily understood in reference to the visual model representation?

\item How well does the outlined approach scale up? How large of a model can be verified before verification time grows to more than 30 minutes on everyday hardware?

\item Can the model generation be modified to capture multiple instances of the CWP concurrently?

\end{enumerate}

Additional questions may be asked as they arise throughout the engineering process.

\subsection{Experiments}



\begin{comment}
A representative set of CWPs and BPMNs needs to be manually drafted in order to perform these experiments. There should be a range of model sizes, as well as models that have very clearly ``unfair'' paths in them. There should be multiple BPMNs that implement a single CWP. There should be BPMNs that have single swimlanes as well as BPMNs with multiple swimlanes. There should be BPMN-CWP pairs that have relatively straightforward state variable pairings as well as BPMN-CWP pairs that have unintuitive pairings. More CWPs and BPMNs provide more data points, which will help to generalize answers to various research questions, including scalability.
\end{comment}

A benchmark set of example CWPs and BPMNs need to be collected before experiments are run. There are two existing CWP/BPMN pairs that will be added to this benchmark set. These are:

\begin{enumerate}

\item COVID-19 remote patient home care, courtesy of \cite{mercer22} and

\item A basic example of an online store to be used as the development example

\end{enumerate}

A third pair will be added to further explore the scalability of this approach. This example will include a feature that can be tuned to be either very small or very large. One potential candidate is a FIFO queue. When answering questions about scalability, I will be collecting model generation time, verification time, and total verification state space. I will continue to scale the model up until model generation time and verification time become larger than about 30 minutes.

It has been shown that a BPMN with three swimlanes, ten activities, eight gateways, and thirteen events can be verified against a CWP in reasonable time \cite{mercer22}. It is anticipated that BPMNs of at least this size can also be verified using an automated tool.

Once the scalability questions are answered, I will begin to explore ideas to allow multiple instances of the CWP to exist concurrently. This may involve changing the BPMN formalization to something else instead of Petri Nets. This paper \cite{wong11} describes two different formalization options for BPMNs that can be explored.

\section{Timeline}

I propose the following tentative timeline for completion of the thesis:

\begin{table}[ht]
\caption{Tentative Project Timeline}
\centering
\scalebox{.8}{
\begin{tabular} {c c c}
\hline \hline
Task & Est. Duration & Est. Completion Date \\
%heading
\hline
(Engineering) Build Code Generation & 1 month & May 15, 2022 \\
(Engineering) Create BPMN and CWP to test scalability & 1 month & June 15, 2022 \\
(Research) Use model to refine research quesitons & 1 month & July 15, 2022 \\
(Research) Answer questions and conclusion & 2 months & Sept. 15, 2022 \\ [1ex]
\end{tabular}}
\label{table:nonlin}
\end{table}


\section{Conclusion}

BPMN is a useful data format for understanding some of the technical details of business processes happening everywhere. They effectively allow us to reason about functional cooperation between human and machine actors. CWPs allow for specification of conceptual work that is worker-agnostic. That is, it can be fulfilled and performed in any number of ways by workers with drastically different performance such as humans and machines. Engineering of a tool for automated evaluation of BPMNs and CWPs would benefit both the business and automated reasoning communities.
\nocite{*}
\bibliography{bibliography}
\end{document}
